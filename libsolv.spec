%bcond_without perl_bindings
%bcond_without ruby_bindings
%bcond_without python3_bindings
%bcond_without appdata
%bcond_without comps
%bcond_without complex_deps
%bcond_without helix_repo
%bcond_without suse_repo
%bcond_without debian_repo
%bcond_without arch_repo
%bcond_without multi_semantics
%bcond_with zchunk
%bcond_without zstd
%bcond_without conda

Name:                  libsolv
Version:               0.7.24
Release:               4
Summary:               Package dependency solver                    
License:               BSD
URL:                   https://github.com/openSUSE/libsolv
Source:                https://github.com/openSUSE/libsolv/archive/refs/tags/%{version}.tar.gz

Patch6001:  backport-Treat-condition-both-as-positive-and-negative-literal-in-pool_add_pos_literals_complex_dep.patch
Patch6002:  backport-Add-testcase-for-last-commit.patch
Patch6003:  backport-choice-rules-also-do-solver_choicerulecheck-for-package-downgrades.patch
Patch6004:  backport-Fix-a-couple-small-static-analysis-findings-for-uninitialized-structs.patch
Patch6005:  backport-resolve-installed-remove-dead-code.patch
Patch6006:  backport-Move-special-updaters-handling-into-its-own-function.patch
Patch6007:  backport-Handle-installed-packages-in-three-passes.patch
Patch6008:  backport-Fix-incomplete-headers.patch
Patch6009:  backport-Fix-a-possible-format-overflow-in-dump_genid.patch

BuildRequires:         cmake gcc-c++ ninja-build pkgconfig(rpm) zlib-devel
BuildRequires:         libxml2-devel xz-devel bzip2-devel 	
%if %{with zstd}
BuildRequires:  libzstd-devel
%endif
%if %{with zchunk}
BuildRequires:  pkgconfig(zck)
%endif

Requires: rpm

%description
A free package dependency solver using a satisfiability algorithm. The
library is based on two major, but independent, blocks:

- Using a dictionary approach to store and retrieve package
  and dependency information.

- Using satisfiability, a well known and researched topic, for
  resolving package dependencies.

%package               devel
Summary:               Development files for %{name}
Requires:              %{name} = %{version}-%{release} rpm-devel

%description devel
Development files for %{name}.

%package tools
Summary:        Package dependency solver tools
Requires:       %{name} = %{version}-%{release}
Requires:       /usr/bin/find

%description tools
Package dependency solver tools.

%package demo
Summary:        Applications demoing the %{name} library
Requires:       %{name} = %{version}-%{release}
Requires:       /usr/bin/curl
Requires:       /usr/bin/gpg2
 
%description demo
Applications demoing the %{name} library.

%package -n            perl-solv
Summary:               Perl bindings for the %{name} library
BuildRequires:         swig perl-devel perl-generators
Requires:              %{name} = %{version}-%{release}

%description -n perl-solv
Perl bindings for the %{name} library.

%package -n            ruby-solv
Summary:               Ruby bindings for the %{name} library
BuildRequires:         swig ruby-devel
Requires:              %{name} = %{version}-%{release}

%description -n ruby-solv
Ruby bindings for the %{name} library.

%package -n            python3-solv
Summary:               Python bindings for the %{name} library
%{?python_provide:%python_provide python3-solv}
BuildRequires:         swig python3-devel
Requires:              %{name} = %{version}-%{release}

%description -n python3-solv
Python bindings for the %{name} library.
Python 3 version.

%package_help

%prep
%autosetup -p1

%build
%cmake . -B"%{_vpath_builddir}" -GNinja          \
  -DFEDORA=1                                     \
  -DENABLE_RPMDB=ON                              \
  -DENABLE_RPMDB_BYRPMHEADER=ON                  \
  -DENABLE_RPMDB_LIBRPM=ON                       \
  -DENABLE_RPMPKG_LIBRPM=ON                      \
  -DENABLE_RPMMD=ON                              \
  %{?with_comps:-DENABLE_COMPS=ON}               \
  %{?with_appdata:-DENABLE_APPDATA=ON}           \
  -DUSE_VENDORDIRS=ON                            \
  -DWITH_LIBXML2=ON                              \
  -DENABLE_LZMA_COMPRESSION=ON                   \
  -DENABLE_BZIP2_COMPRESSION=ON                  \
  %{?with_zstd:-DENABLE_ZSTD_COMPRESSION=ON}     \
%if %{with zchunk}
  -DENABLE_ZCHUNK_COMPRESSION=ON                 \
  -DWITH_SYSTEM_ZCHUNK=ON                        \
%endif
  %{?with_helix_repo:-DENABLE_HELIXREPO=ON}      \
  %{?with_suse_repo:-DENABLE_SUSEREPO=ON}        \
  %{?with_debian_repo:-DENABLE_DEBIAN=ON}        \
  %{?with_arch_repo:-DENABLE_ARCHREPO=ON}        \
  %{?with_multi_semantics:-DMULTI_SEMANTICS=ON}  \
  %{?with_complex_deps:-DENABLE_COMPLEX_DEPS=1}  \
  %{?with_conda:-DENABLE_CONDA=ON}               \
  %{?with_perl_bindings:-DENABLE_PERL=ON}        \
  %{?with_ruby_bindings:-DENABLE_RUBY=ON}        \
%if %{with python3_bindings}
  -DENABLE_PYTHON3=ON                            \
  -DPYTHON_EXECUTABLE=%{__python3}               \
%endif
  %{nil}
%ninja_build -C "%{_vpath_builddir}"

%install
%ninja_install -C "%{_vpath_builddir}"

%check
%ninja_test -C "%{_vpath_builddir}"

%if %{undefined ldconfig_scriptlets}
%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig
%else
%ldconfig_scriptlets
%endif

%files
%license LICENSE*
%{_libdir}/%{name}.so.*
%{_libdir}/%{name}ext.so.*

%files devel
%{_libdir}/%{name}.so
%{_libdir}/%{name}ext.so
%{_includedir}/solv/
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/pkgconfig/%{name}ext.pc
%dir %{_datadir}/cmake/Modules/
%{_datadir}/cmake/Modules/FindLibSolv.cmake

%files tools
%{_bindir}/deltainfoxml2solv
%{_bindir}/dumpsolv
%{_bindir}/installcheck
%{_bindir}/mergesolv
%{_bindir}/repomdxml2solv
%{_bindir}/rpmdb2solv
%{_bindir}/rpmmd2solv
%{_bindir}/rpms2solv
%{_bindir}/testsolv
%{_bindir}/updateinfoxml2solv
%{_bindir}/repo2solv
%if %{with comps}
  %{_bindir}/comps2solv
%endif
%if %{with appdata}
  %{_bindir}/appdata2solv
%endif
%if %{with debian_repo}
  %{_bindir}/deb2solv
%endif
%if %{with arch_repo}
  %{_bindir}/archpkgs2solv
  %{_bindir}/archrepo2solv
%endif
%if %{with helix_repo}
  %{_bindir}/helix2solv
%endif
%if %{with suse_repo}
  %{_bindir}/susetags2solv
%endif
%if %{with conda}
  %{_bindir}/conda2solv
%endif

%files demo
%{_bindir}/solv

%files -n perl-solv
%{perl_vendorarch}/solv.pm
%{perl_vendorarch}/solv.so

%files -n ruby-solv
%{ruby_vendorarchdir}/solv.so

%files -n python3-solv
%{python3_sitearch}/*

%files help
%doc README
%{_mandir}/man1/*
%{_mandir}/man3/%{name}*.3*

%changelog
* Tue Jul 23 2024 zhangxingrong <zhangxingrong@uniontech.cn> - 0.7.24-4
- Fix incomplete headers
- Fix a possible format overflow in dump_genid

* Fri Jul 5 2024 guojunding <guojunding@kylinos.cn> - 0.7.24-3
- Handle installed packages in three passes

* Tue Jun 18 2024 guojunding <guojunding@kylinos.cn> - 0.7.24-2
- Fix a couple small static analysis findings for uninitialized structs

* Sun Feb 4 2024 hanhuihui<hanhuihui5@huawei.com> - 0.7.24-1
- DESC:update libsolv to 0.7.24-1

* Sat Aug 19 2023 hanhuihui<hanhuihui5@huawei.com> - 0.7.22-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix requirement error

* Tue Nov 8 2022 hanhuihui<hanhuihui5@huawei.com> - 0.7.22-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update libsolv to 0.7.22-1

* Fri Oct 21 2022 hanhuihui<hanhuihui5@huawei.com> - 0.7.20-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:ensure-duplinvolvedmap_all-is-reset.patch

* Tue Oct 11 2022 zhangjun<zhangjun@kylinos.cn> - 0.7.20-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix memory leak when using testsolv to execute cases 
       Fix segfault on conflict resolution when using bindings

* Sat Dec 25 2021 wangkerong<wangkerong@huawei.com> - 0.7.20-1
- update to 0.7.20

* Sat Jan 30 2021 xihaochen <xihaochen@huawei.com> - 0.7.17-1
- Type:requirements                                                                                                                                  
- Id:NA
- SUG:NA
- DESC:update libsolv to 0.7.17

* Tue Aug 4 2020 linwei<linwei54@huawei.com> - 0.7.14-2
- modify spec for requires

* Wed Jul 29 2020 linwei<linwei54@huawei.com> - 0.7.14-1
- Update libsolv to 0.7.14

* Tue Jan 7 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.7.7-2 
- Pakcage init
